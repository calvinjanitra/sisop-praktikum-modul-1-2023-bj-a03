#!/bin/bash

dir="$( cd "$( dirname "$0" )" && pwd )"

filedir="$dir/users/users.txt"

echo "Login Menu"
read -p "Username: " user
read -p "Password: " pass

if grep -q "^${user}=" "$filedir";
then
    if grep -q "^${user}=${pass}$" "$filedir";
    then
        echo "Login berhasil"
        echo "$(date '+%Y/%m/%d %H:%M:%S') LOGIN: User $(user) logged in" >> "$dir/log.txt"
    else
        echo "Password salah"
    fi
else  
    echo "$(date '+%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on User $(user)" >> "$dir/log.txt"
    echo "Akun belum terdaftar"
fi
