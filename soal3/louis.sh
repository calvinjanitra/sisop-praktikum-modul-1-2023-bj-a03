#!/bin/bash

dir="$( cd "$( dirname "$0" )" && pwd )"

if [ ! -f $dir/users/users.txt ]
then
mkdir $dir/users
touch $dir/users/users.txt
fi

filedir="$dir/users/users.txt"
user_list=$(cat $filedir)
echo $user_list

echo "Regristration Menu"
read -p "Username: " new_user
read -p "Password: " new_pass


if [[ ${#new_pass} -ge 8 ]]
then

    if [[ $(echo "$new_pass" | grep '[A-Z]' | grep '[a-z]') ]]
    then
        if [[ "$new_pass" != "$new_user" ]]
        then
            if [[ ! $(echo "$new_pass" | grep -i 'chicken') ]] && [[ ! $(echo "$new_pass" | grep -i 'ernie') ]]; 
            then
                if [[ "$new_pass" =~ ^[[:alnum:]]+$ ]]
                then
                    if [[ "$user_list" =~ "$new_user" ]]
                    then
                        echo "Nama pengguna sudah diambil"
                        echo "$(date '+%Y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> "$dir/log.txt"
                    else 
                        echo "$new_user=$new_pass" >> "$filedir"
                        echo "Akun berhasil dibuat"
                        echo "$(date '+%Y/%m/%d %H:%M:%S') REGISTER: INFO User $(new_user) registered successfully" >> "$dir/log.txt"
                    fi
                else
                    echo "Password hanya dapat mengandung huruf atau angka"
                fi
            else
                echo "Password tidak boleh mengandung kata chicken atau ernie"
            fi
        else
            echo "Password tidak boleh sama dengan username"
        fi
    else
        echo "Password harus memiliki 1 huruf kecil dan 1 huruf besar"
    fi
else
    echo "Password harus memiliki minimal 8 karakter"
fi






