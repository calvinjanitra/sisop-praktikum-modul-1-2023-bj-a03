#!/bin/bash

echo "Please enter a file to be decrypt: "
read name

hour=$(echo "$name" | cut -c1-2)
int_hour=$((hour))

start=$((int_hour-1))
end=$((start-1))

#echo $start

lowcase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
upcase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

de_s1="${lowcase[$start]}"
de_e1="${lowcase[$end]}"
de_s2="${upcase[$start]}"
de_e2="${upcase[$end]}"

echo "$de_s1 $de_e1 $de_s2 $de_e2"

de_low="$de_s1-za-$de_e1"
de_up="$de_s2-ZA-$de_e2"

echo "$de_low $de_up"

log=$(cat "$name")

de_log=$(echo "$log" | tr $de_up 'A-Z' | tr $de_low 'a-z')

echo $de_log 

