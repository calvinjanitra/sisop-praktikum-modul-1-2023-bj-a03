#!/bin/bash

dir="$( cd "$( dirname "$0" )" && pwd )"
name=$(date "+%H:%M_%d:%m:%y")

cur_h=$(date +%H)
start=$((cur_h-1))
end=$((cur_h-2))

#echo $cur_h

lowcase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
upcase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

en_s1="${lowcase[$start]}"
en_e1="${lowcase[$end]}"
en_s2="${upcase[$start]}"
en_e2="${upcase[$end]}"

#echo "$en_s1 $en_e1 $en_s2 $en_e2"

en_low="$en_s1-za-$en_e1"
en_up="$en_s2-ZA-$en_e2"

log=$(cat "/var/log/syslog")

en_log=$(echo "$log" | tr 'A-Z' $en_up | tr 'a-z' $en_low)

# echo $en_log > "$dir/$name.txt" 

# touch cobacron

# echo "0 */2 * * * bash $dir/log_encrypt.sh" > cobacron

# crontab cobacron
# rm -rf cobacron