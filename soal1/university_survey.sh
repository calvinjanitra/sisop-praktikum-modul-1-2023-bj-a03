#!/bin/bash

echo "**No 1a**"
echo
grep "Japan" "2023 QS World University Rankings.csv" | awk 'NR<=5 {print}' | column -s, -t
echo
echo "**No 1b**"
echo
echo "5 Univ dengan fsr terendah di jepang"
grep "Japan" "2023 QS World University Rankings.csv"  | sort -t, -V -k9 | awk 'NR<=5 {print}' |  column -s, -t
echo
echo "Univ dengan fsr paling rendah di jepang"
grep "Japan" "2023 QS World University Rankings.csv"  | sort -t, -V -k9 | awk 'NR<=1 {print}' |  column -s, -t
echo
echo "**No 1c**"
echo
grep "Japan" "2023 QS World University Rankings.csv"  | sort -t, -k20n | awk 'NR<=10 {print}' |  column -s, -t
echo
echo "**No 1d**"
echo
grep "Keren" "2023 QS World University Rankings.csv"  | awk ' {print}' | column -s, -t

