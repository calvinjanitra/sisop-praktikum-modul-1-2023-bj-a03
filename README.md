# Sisop Praktikum Modul 1 2023 BJ A03

## Kelompok A03

Nomer | Nama | NRP |
| ------------- | ------------- |------------- |
1 | Calvin Janitra | 5025211020 |
2 | Andrian | 5025211079 |
3 | Dafarel Fatih Wirayudha| 5025211120 |

## Soal 1
##### Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi

**Code :** 
```bash
#!/bin/bash

echo "**No 1a**"
echo
grep "Japan" "2023 QS World University Rankings.csv" | awk 'NR<=5 {print}' | column -s, -t
echo
echo "**No 1b**"
echo
echo "5 Univ dengan fsr terendah di jepang"
grep "Japan" "2023 QS World University Rankings.csv"  | sort -t, -V -k9 | awk 'NR<=5 {print}' |  column -s, -t
echo
echo "Univ dengan fsr paling rendah di jepang"
grep "Japan" "2023 QS World University Rankings.csv"  | sort -t, -V -k9 | awk 'NR<=1 {print}' |  column -s, -t
echo
echo "**No 1c**"
echo
grep "Japan" "2023 QS World University Rankings.csv"  | sort -t, -k20n | awk 'NR<=10 {print}' |  column -s, -t
echo
echo "**No 1d**"
echo
grep "Keren" "2023 QS World University Rankings.csv"  | awk ' {print}' | column -s, -t
```

#### A.
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

**Code :** 
```bash
grep "Japan" rankings.csv | awk 'NR<=5 {print}' | column -s, -t
```

**Penjelasan :**
```bash
grep "Japan" rankings.csv
```
grep digunakan untuk mencari pola tertentu pada sebuah file, dalam hal ini adalah kata "Japan" di dalam file rankings.csv

```bash
awk 'NR<=5 {print}'
```
mengeluarkan output dengan 5 data teratas

```bash
column -s, -t
```
memformat output sebagai tabel dengan separator koma
#### B.
Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang.

**Code :**
```bash
grep "Japan" "2023 QS World University Rankings.csv"  | sort -t, -V -k9 | awk 'NR<=1 {print}' |  column -s, -t
```

**Penjelasan :**
```bash
grep "Japan" rankings.csv 
```
Mencari baris yang mengandung kata "Japan" di file rankings.csv

```bash
sort -t, -V -k9
```
Mengurutkan data dengan separator koma berdasarkan kolom ke-9 secara numerical order menggunakan -V dan urut secara ascending

```bash
awk 'NR<=1 {print}'
```
mengeluarkan output dengan 1 data teratas

```bash
column -s, -t
```
memformat output sebagai tabel dengan separator koma
#### C.
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

**Code :**
```bash
grep "Japan" rankings.csv | sort -t, -k20n | awk 'NR<=10 {print}' |  column -s, -t
```

**Penjelasan :**
```bash
grep "Japan" rankings.csv
```
Mencari baris yang mengandung kata "Japan" di file rankings.csv

```bash
sort -t, -k20n
```
Mengurutkan data dengan separator koma berdasarkan kolom ke-20 secara ascending

```bash
awk 'NR<=10 {print}'
```
mengeluarkan output dengan 10 data teratas

```bash
column -s, -t
```
Memformat output sebagai tabel dengan separator koma
#### D.
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

**Code :**
```bash
grep "Keren" rankings.csv | awk ' {print}' | column -s, -t
```

**Penjelasan :**
```bash
grep "Keren" rankings.csv
```
Mencari baris yang mengandung kata "Keren" di file rankings.csv

```bash
awk ' {print}'
```
Memisahkan data dengan separator koma lalu {print} untuk mengeluarkan output

```bash
column -s, -t
```
Memformat output sebagai tabel dengan separator koma

## Soal 2
##### Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

#### A.
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan

**Solusi :**

**Code Download :**
```bash
function download(){
    index=1
    while [ -d kumpulan_$index ]
    do
        index=$((index+1))
    done
    mkdir kumpulan_$index
    cd kumpulan_$index

    hour=$(date +"%H")
        # echo $hour

    if [[ $hour -eq 0 && $(date +"%M") -eq 0 ]]
    then
        hour=1
    fi
    for ((i=1; i<=$hour; i++))
    do
        wget -O perjalanan_$i.jpg https://pict.sindonews.net/dyn/620/pena/news/2020/12/06/34/258298/lewat-we-love-bali-pulau-dewata-pastikan-siap-sambut-wisatawan-hsv.jpg
    done
}
```

**Penjelasan :**
```bash
    index=1
    while [ -d kumpulan_$index ]
    do
        index=$((index+1))
    done
```
Code diatas digunakan untuk melakukan iterasi directory (-d) dengan nama kumpulan_$index. Variabel index dimulai dari angka 1. Jika iterasi benar, maka masuk ke dalam "do" untuk di increment


```bash
    mkdir kumpulan_$index
    cd kumpulan_$index
```
Code diatas digunakan untuk membuat folder dengan "mkdir" dengan nama kumpulan_$index. Lalu kita masuk ke folder dengan menggunakan cd (change directory)


```bash
    hour=$(date +"%H")
```
Function date disini kita masukkan ke dalam variabel "hour" untuk mengambil pukul berapa saat script dijalankan.

```bash
    if [[ $hour -eq 0 && $(date +"%M") -eq 0 ]]
    then
        hour=1
    fi
```
Conditional diatas digunakan sesuai kasus soal yaitu ketika jam = 0 dan menit = 0, maka dilakukan download gambar 1x saja.

```bash
    for ((i=1; i<=$hour; i++))
    do
        wget -O perjalanan_$i.jpg https://pict.sindonews.net/dyn/620/pena/news/2020/12/06/34/258298/lewat-we-love-bali-pulau-dewata-pastikan-siap-sambut-wisatawan-hsv.jpg
    done
```
For loop diatas digunakan untuk melakukan download dengan batas sebanyak variabel hour. Download dilakukan dengan function "wget" dan -O digunakan untuk menamai file output.

#### B.
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

**Solusi :**

**Code Zip :**
```bash
function zipp(){
    index=1
    while [ -d kumpulan_$index ]
    do 
        if [ -f devil_$index.zip ]
        then
            index=$((index+1))
            continue
        fi

        zip -r devil_$index.zip kumpulan_$index
        index=$((index+1))
    done
}
```

**Penjelasan :**
```bash
    index=1
    while [ -d kumpulan_$index ]
```
While diatas digunakan untuk melakukan iterasi folder directory yang bernama kumpulan_$index.

```bash
    do 
        if [ -f devil_$index.zip ]
        then
            index=$((index+1))
            continue
        fi


    done
```
Jika while benar, maka masuk ke dalam conditional if. Di dalam if kita melakukan pencarian dengan -f apakah ada file dengan nama devil_$index.zip. Jika ada, maka index akan diincrement dan keluar dari if.

```bash
    zip -r devil_$index.zip kumpulan_$index
    index=$((index+1))
```
Jika file dengan nama devil_$index.zip tidak ditemukan, maka akan dizip dengan function zip -r. -r disini memiliki arti rekursif yaitu function zip akan melakukan zip berulang kali secara rekursi ke dalam file-file di dalam directory

```bash
if [ $1 -eq 1 ]
then
    download
elif [ $1 -eq 2 ]
then
    zipp
else
    echo "Please input 1(Download)/2(Zip)"    
fi
```
Code diatas saya gunakan untuk membuat parameter ketika file di bash nanti saat crontab. Jika parameter 1, maka melakukan download. Jika parameter 2, lakukan zip

#### Configurasi Crontab
```bash
Download
MAILTO=""
* */24 * * * /bin/bash /home/calvinjanitra/Sisop/kobeni_liburan.sh 1

Zip
* */10 * * * /bin/bash /home/calvinjanitra/Sisop/kobeni_liburan.sh 2

```

#### ERROR
- Konfigurasi crontab error karena belum menambahkan MAILTO dan /bin.bash


## Soal 3
##### Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

- Sistem Registrasi 
Dalam melakukan regristrasi akun, terdapat beberapa persyratan, yaitu:
1. Password
    - Minimal 8 karakter
        Untuk mengecek kondisi ini, kita dapat #variabel untuk mencari jumlah huruf suatu variabel.
        ```bash
        if [[ ${#new_pass} -ge 8 ]]
        ```
    - Memiliki setidaknya 1 huruf kecil dan huruf besar
        Kita dapat menggunakan grep untuk mencari huruf kecil dan huruf besar.
        ```bash
        if [[ $(echo "$new_pass" | grep '[A-Z]' | grep '[a-z]') ]]
        ```
    - Password tidak boleh sama seperti username
        Kita dapat menggunakan operator != untuk membandingkan kedua nilai tersebut.
        ```bash
        if [[ "$new_pass" != "$new_user" ]]
        ```
    - Password tidak menggandung kata 'chicken' atau 'ernie'
        Seperti dengan cara mencari 1 huruf kecil dan 1 huruf besar, kita juga dapat menggunakan grep untuk mencari 1 kata yang terdapat pada suatu variabel. Lalu, untuk memenuhi persyaratan di atas, kita perlu negasi ekspresi tersebut.
        ```bash
        if [[ ! $(echo "$new_pass" | grep -i 'chicken') ]] && [[ ! $(echo "$new_pass" | grep -i 'ernie') ]]; 
        ```
    - Password berupa alphanumeric
        Untuk mengecek apakah password yang diketik berupa alphanumeric, kita dapat menggunakan operasi =~ dimana ini akan membandingkan string pada sisi kiri dengan kanan. Password kemudian akan dibandingkan dengan eksprisi yang memuat hanya angka dan huruf.
        ```bash
        if [[ "$new_pass" =~ ^[[:alnum:]]+$ ]]
        ```
2. Username
    - Username yang akan dibuat tidak boleh sama dengan yang sudah terdaftar
        Disini juga menggunakan operasi perbandingan =~ 
        ```bash
            if [[ "$user_list" =~ "$new_user" ]]
        ```
    - Ketika semua persyaratan sudah penuhi, username dan password akan dicatat pada suatu textfile.
    ```bash
        echo "$new_user=$new_pass" >> "$filedir"
    ```

    - Sistem login
        Kita dapat menggunakan grep untuk mencari username pada textfile yang berisi akun-akun yang telah terdaftar. Kali ini, kita menggunakan grep dengan mencari suatu pattern berupa format yang ditentukan.
        ```bash
        if grep -q "^${user}=" "$filedir";
        if grep -q "^${user}=${pass}$" "$filedir";
        ```
    - Pencatatan pada log.
        Untuk semua pencatatan pada log, berada pada bagian if masing-masing dengan format date yang sama serta informasi aktivitas pengguna.
        ```bash
            #login
            echo "$(date '+%Y/%m/%d %H:%M:%S') LOGIN: User $(user) logged in" >> "$dir/log.txt"
            echo "$(date '+%Y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on User $(user)" >> "$dir/log.txt"
            
            #regristrasi
            echo "$(date '+%Y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> "$dir/log.txt"
            echo "$(date '+%Y/%m/%d %H:%M:%S') REGISTER: INFO User $(new_user) registered successfully" >> "$dir/log.txt"
        ```

#### ERROR
- Fungsi grep tiba-tiba tidak berfungsi, jadinya pakai =~
- mkdir pada directory file yg sama dengan shell kemarin tidak bisa langsung, jadi harus di state dulu directory nya


## Soal 4
##### Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 

- Enkrypt

Pada proses enkripsi dengan metode cipher, kita dapat menggunakan syntax dari tr (transform) untuk mengubah interval A-Z.
contoh : A-Z ditransform sebanyak 5 akan mengahasilkan interval baru, yaitu E-D.

Interval lama: ABCDEFGHIJKLMNOPQRSTUVWXYZ

Interval baru: EFGHIJKLMNOPQRSTUVWXYZABCD


STEP BY STEP CODE:
1. Membuat variabel yang akan menampung nama text file tersebut.
note: format jam:menit tanggal:bulan:tahun 
```bash
name=$(date "+%H:%M_%d:%m:%y")
```
2. Membuat variabel yang akan menampung jam sekarang.
```bash
cur_h=$(date +%H)
```
3. Mencari index interval baru setelah dienkripsi
```bash
#index mulai dari 0
start=$((cur_h-1))
end=$((cur_h-2))
```
4. Mendeclare 2 array yang nantinya akan dienkripsi: 
- lowcase(berisi alphabet huruf kecil)
- upcase(berisi alphabet huruf besar)
```bash
lowcase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
upcase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
```

5. Membuat interval baru dan digabungkan pada variabel.
```bash
en_s1="${lowcase[$start]}"
en_e1="${lowcase[$end]}"
en_s2="${upcase[$start]}"
en_e2="${upcase[$end]}"

en_low="$en_s1-za-$en_e1"
en_up="$en_s2-ZA-$en_e2"
```
6. Membuat variable yang akan menampung isi dari syslog.
```bash
log=$(cat "/var/log/syslog")
```
7. Melakukan shifting dengan tr(transform).
```bash
en_log=$(echo "$log" | tr 'A-Z' $en_up | tr 'a-z' $en_low)
```
8. Menulis hasil tr(transform) pada sebuah text file dengan format yang sudah ditentukan.
```bash
echo $en_log > "$HOME/Documents/sisop/1/backup/$name.txt" 
```
<br>

- Decrypt

Sama seperti proses enkripsi, pada proses dekripsi, kita juga dapat menggunakan tr untuk mengembalikan interval baru kembali ke interval lama.
contoh: E-D ditransform sebanyak 5 akan mengahasilkan interval baru, yaitu A-Z.

Interval lama: EFGHIJKLMNOPQRSTUVWXYZABCD

Interval baru: ABCDEFGHIJKLMNOPQRSTUVWXYZ

STEP BY STEP CODE

1. Membuat sebuah variabel untuk menampung inputan user (nama file yang ingin didekripsi).
bash
```echo "Please enter a file to be decrypt: "
read name
```
2. Mengambil 2 huruf pertama dan convert merupakan jam dimana file tersebut terbentuk. Lalu, convert variabel itu menjadi bentuk integer.
```bash
hour=$(echo "$name" | cut -c1-2)
int_hour=$((hour))
```
3. Mencari index interval baru setelah dienkripsi
```bash
#index mulai dari 0
start=$((int_hour-1))
end=$((start-1))
```
4. Mendeclare 2 array yang nantinya akan dienkripsi: 
- lowcase(berisi alphabet huruf kecil)
- upcase(berisi alphabet huruf besar)
```bash
lowcase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
upcase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
```

5. Membuat interval baru dan digabungkan pada variabel.
```bash
de_s1="${lowcase[$start]}"
de_e1="${lowcase[$end]}"
de_s2="${upcase[$start]}"
de_e2="${upcase[$end]}"

de_low="$de_s1-za-$de_e1"
de_up="$de_s2-ZA-$de_e2"
```
6. Membuat variable yang akan menampung isi dari file yang telah ditentukan.
```bash
log=$(cat "$name")
```
7. Melakukan shifting dengan tr(transform).
```bash
de_log=$(echo "$log" | tr $de_up 'A-Z' | tr $de_low 'a-z')
```
8. Menampilkan hasil tr(transform).
```bash
echo $de_log
```

#### ERROR
- Crontab tidak berfungsi
- Soal yang ambigu terkait syslog
- File yg dihasilkan tidak berada pada directory shell
