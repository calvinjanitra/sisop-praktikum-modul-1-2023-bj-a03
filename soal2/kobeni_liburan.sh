#!/bin/bash

cd "/home/calvinjanitra/Sisop"

function download(){
    index=1
    while [ -d kumpulan_$index ]
    do
        index=$((index+1))
    done
    mkdir kumpulan_$index
    cd kumpulan_$index

    hour=$(date +"%H")
        # echo $hour

    if [[ $hour -eq 0 && $(date +"%M") -eq 0 ]]
    then
        hour=1
    fi
    for ((i=1; i<=$hour; i++))
    do
        wget -O perjalanan_$i.jpg https://pict.sindonews.net/dyn/620/pena/news/2020/12/06/34/258298/lewat-we-love-bali-pulau-dewata-pastikan-siap-sambut-wisatawan-hsv.jpg
    done
}

function zipp(){
    index=1
    while [ -d kumpulan_$index ]
    do 
        if [ -f devil_$index.zip ]
        then
            index=$((index+1))
            continue
        fi

        zip -r devil_$index.zip kumpulan_$index
        index=$((index+1))
    done
}

if [ $1 -eq 1 ]
then
    download
elif [ $1 -eq 2 ]
then
    zipp
else
    echo "Please input 1(Download)/2(Zip)"    
fi

//cronjob
//download
# MAILTO=""
# * */24 * * * /bin/bash /home/calvinjanitra/Sisop/kobeni_liburan.sh 1
//zip
# * */10 * * * /bin/bash /home/calvinjanitra/Sisop/kobeni_liburan.sh 2
